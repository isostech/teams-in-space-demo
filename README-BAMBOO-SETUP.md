## Bamboo Specs set up procedure

### Set up environment

1. Install [Docker](https://docs.docker.com/docker-for-mac/install/)
2. Configure Docker so it can access local file system: go _Preferences / File Sharing_, add _/opt/atlassian/data_
folder
3. (re)start the Docker deamon
4. install [IntelliJ Idea](https://www.jetbrains.com/idea/download/#section=mac)

### Warm up

1. run _docker run golang_ locally, so docker images are cached locally

## Run the demo

### Show IDE driven Bamboo Specs

1. clone [apollo-ui](http://bitbucket.teamsinspace.com:7990/projects/TIS/repos/apollo-ui/browse) repository locally,
open bamboo-specs folder as Maven project in IDE, run it
2. expect [Apollo UI (Specs)](http://bamboo.teamsinspace.com:8085/browse/TIS-AUISPECS) Bamboo build plan created/updated

### Show Bitbucket Server stored Bamboo Specs

1. Add new [Bamboo Linked](http://bamboo.teamsinspace.com:8085/admin/configureLinkedRepositories!doDefault.action)
repository _Teams in Space / Apollo UI_ as a Bitbucket Server/Stash repository type.
2. enable Bamboo Specs for newly created Linked Repository - click _Bamboo Specs_ tab, toggle _Scan for Bamboo Specs_ on
3. set permissions on the same tab, tick _Access all projects_ checkbox
4. Make any change to
[bamboo-specs folder of Teams in Space / Apollo UI BBS repository](http://bitbucket.teamsinspace.com:7990/projects/TIS/repos/apollo-ui/browse/bamboo-specs)
(e.g. add empty line to
[pom.xml](http://bitbucket.teamsinspace.com:7990/projects/TIS/repos/apollo-ui/browse/bamboo-specs/pom.xml),
can use BBS inline editor) so Bamboo detects a change
5. expect [Apollo UI (Specs)](http://bamboo.teamsinspace.com:8085/browse/TIS-AUISPECS) Bamboo build plan created/updated

### Show Bitbucket Server stored bitbucket-pipelines.yml specs
__Note this is experimental feature, we can demo a concept but can't guarantee paritcular file syntax at this stage__

1. Add new [Bamboo Linked](http://bamboo.teamsinspace.com:8085/admin/configureLinkedRepositories!doDefault.action)
repository _Teams in Space / pipelines-example-go_ as a Bitbucket Server/Stash repository type.
2. enable Bamboo Specs for newly created Linked Repository - click _Bamboo Specs_ tab, toggle _Scan for Bamboo Specs_ on
3. set permissions on the same tab, tick _Access all projects_ checkbox
4. Make any change to
[bitbucket-pipelines.yml file of Teams in Space / pipelines-example-go BBS repository](http://bitbucket.teamsinspace.com:7990/projects/TIS/repos/pipelines-examples-go/browse/bitbucket-pipelines.yml)
(e.g. add empty line to that file, can use BBS inline editor) so Bamboo detects a change
5. expect _Pipelines of pipelines-example Pipeline_ plan created/updated in
[Bamboo](http://bamboo.teamsinspace.com:8085/allPlans.action)
