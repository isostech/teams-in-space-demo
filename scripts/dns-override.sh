#!/bin/bash

# Override *.teamsinspace.com A record from 127.0.0.1 to a given IP.
# This script is a plan B, in case there are multiple demo hosts are available on the same network and you intend to use some of them as clients only.
# Contact: pkoczan@atlassian.com

if [ ! -w /etc/hosts ]; then
	echo "This script needs to be run as an user with write permissions on /etc/hosts - you are not one of those. Exiting"
	exit 1;
fi

function valid_ip() # Source: http://www.linuxjournal.com/content/validating-ip-address-bash-script
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

RESET=false;

while [ "${1+defined}" ]; do
	if [ "$1" == "-r" ]; then
		RESET=true;
	fi
	shift
done



if [ $RESET = true ]; then
	echo "Resetting *.teamsinspace.com DNS entries"
	grep -v "teamsinspace.com" /etc/hosts >/tmp/hosts.temp
	cat /tmp/hosts.temp >/etc/hosts
	rm -rf /tmp/hosts.temp
	exit 0;
fi

echo "DNS record override for *.teamsinspace.com. Use $0 -r to reset the changes"
read -p "IP address to be used for *.teamsinspace.com: " NEWIP

if ! valid_ip $NEWIP; then
	echo "Invalid IP address, exiting."
	exit 1;
fi

grep -v "teamsinspace.com" /etc/hosts >/tmp/hosts.temp
echo "$NEWIP bamboo.teamsinspace.com confluence.teamsinspace.com crowd.teamsinspace.com fecru.teamsinspace.com jira.teamsinspace.com bitbucket.teamsinspace.com" >>/tmp/hosts.temp
cat /tmp/hosts.temp >/etc/hosts
rm -rf /tmp/hosts.temp

echo "Finished. You can verify by pinging jira.teamsinspace.com for example."
exit 0;

