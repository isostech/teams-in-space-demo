#!/bin/bash

apt-get update -y
apt-get install -y zip unzip

export PACKER_VERSION="0.9.0" #SHould remove this and set it in pipelines environment vars
git clone davidglennjenkins@bitbucket.org:davidglennjenkins/packer-aws.git

echo "===> Installing Packer and cleanup..."                                                                 && \
rm -rf /usr/sbin/packer                                                                                      && \
curl -LO https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip    && \
unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/bin/                                                  && \
rm -rf packer_${PACKER_VERSION}_linux_amd64.zip                                                              && \

cd packer-aws && packer --version
